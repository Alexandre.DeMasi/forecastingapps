# Forecasting Smartphone Application Chains: an App-Rank Based Approach

Alexandre De Masi and Katarzyna Wac. 2023. Forecasting Smartphone Application Chains: an App-Rank Based Approach. In Proceedings of the 22nd International Conference on Mobile and Ubiquitous Multimedia (MUM '23). Association for Computing Machinery, New York, NY, USA, 81–92. https://doi.org/10.1145/3626705.3627802

     @inproceedings{10.1145/3626705.3627802,
     author = {De Masi, Alexandre and Wac, Katarzyna},
     title = {Forecasting Smartphone Application Chains: An App-Rank Based Approach},
     year = {2023},
     isbn = {9798400709210},
     publisher = {Association for Computing Machinery},
     address = {New York, NY, USA},
     url = {https://doi.org/10.1145/3626705.3627802},
     doi = {10.1145/3626705.3627802},
     abstract = {Research indicates that smartphone users reuse the same applications throughout the day. This study aimed to forecast a list of probable applications to be launched on a smartphone based on prior usage patterns, without the use of contextual information. We proposed a ranked-based algorithm that considers the sequential behavior of application usage history and presents usage sessions as "application chains". We evaluated the algorithm using datasets from 397 users, comprising 433,663 application chains with a minimum of three applications and 174 applications for the longest chains, averaging 40.91 ± 18.76 applications per chains, recorded over varying time periods and across multiple countries. Our results indicate that the proposed algorithm outperforms alternative approaches, achieving a significantly higher F1 score of 62 ± 6\% without the use of contextual information. The ability to predict application launch can enable the provision of additional services such as digital wellness and improved application’s Quality of Experience.},
     booktitle = {Proceedings of the 22nd International Conference on Mobile and Ubiquitous Multimedia},
     pages = {81–92},
     numpages = {12},
     keywords = {Experience, Interaction, Forecasting, Method, Smartphone, Application},
     location = {, Vienna, Austria, },
     series = {MUM '23}
    }

     
[Get the paper](https://doi.org/10.1145/3626705.3627802)

## Jupyter Notebook Scripts

- 02-MPU_to_Seq: Implementation of the data wrangling, filtering and application session.
- 03-Dummy_*: Dummy models implementations
- 04-LSTM_TimeBaseCV: Seq2Seq model
- 04-XBG_TimeBaseCV: Ranking model
- helper.py: Helper functions
- TimeBaseCV: Cross-validator base on time (e.g. 7 days)
