from pandas import DataFrame
from pandas import read_feather
from pandas import read_pickle
from pandas import Timedelta
from pandas import read_csv
from pandas import to_datetime
from pathlib import Path 
import numpy as np
from scipy import stats


def pre_pross_mpu(path_:str) -> [DataFrame,DataFrame,int]:
    df = read_csv(path_)
    keep_scr=["ts","Screen_Value"]
    keep_app=["ts","App_Value"]
    app = df[keep_app]
    df= df[keep_scr]
    app["ts"]=to_datetime(app['ts'])
    df["ts"]=to_datetime(df['ts'])
    app.set_index('ts',inplace=True)
    app.dropna(inplace=True)
    df.set_index('ts',inplace=True)
    df.dropna(inplace=True)
    app.rename(columns={'App_Value':'process_name'},inplace=True)
    df.rename(columns={'Screen_Value':'action'},inplace=True)
    df.sort_index(inplace=True)
    uid = Path(path_).name.split('.')[0]
    return df, app, uid

def sessionmpu(df : DataFrame) -> [DataFrame,int,int]:
    action = ['Unlocked', 'Off']
    df = df[df.action.apply(lambda x: x in action)]
    if (df.iloc[0].action == "Off"):df = df[1:]
    df['activity_next']=df.action.shift(-1)
    df['activity_prev']=df.action.shift(1)
    df['on_time'] = df.index 
    df['off_time']=df.on_time.shift(-1)
    df =df[ (df.action.str.contains("Unlocked") & (df.activity_next.str.contains("Off")))]
    df['delta_session'] =  df.off_time -df.on_time
    df['delta_session'] = df.delta_session.dt.total_seconds()

    df["z"] = np.abs(stats.zscore(df['delta_session']))
    prez= df.shape[0]
    df=df[df['z'] <3]
    postz= df.shape[0]
    return df,prez,postz

def appclean(df : DataFrame) -> [DataFrame,int,int,int]:
    pre= df.shape[0]
    app_rem=["com.android.systemui","android","null","com.android.launcher3","com.google.android.packageinstaller",\
        "com.google.android.googlequicksearchbox","com.google.android.inputmethod.latin","com.touchtype.swiftkey",\
        "com.google.android.apps.nexuslauncher","com.android.vending","com.google.android.permissioncontroller",\
        "com.android.settings"]
    df = df[df.process_name.apply(lambda x: x not in app_rem )]
    df = df[~df.process_name.str.startswith('-')]
    df = df[~df.process_name.str.startswith('+')]
    post= df.shape[0]
    return df,pre,post,len(df.process_name.unique())

def appchain(app:DataFrame,start:str,end:str,session:int) -> DataFrame:
    chain = app.truncate(before=start,after=end)
    try:
        chain.drop(columns="uid",inplace=True)
    except:
        pass
    chain['sessionid'] = chain['process_name'].ne(chain['process_name'].shift().bfill()).astype(int)
    if chain.shape[0]<1:
        return DataFrame()
    chain.at[chain.iloc[0].name,"sessionid"]=1
    chain = chain.loc[chain.sessionid == 1]
    chain["stop"] = chain.index
    chain["stop"] = chain.stop.shift(-1)
    chain.at[chain.iloc[-1].name,"stop"]=end
    chain["sessionid"]=session
    return chain

