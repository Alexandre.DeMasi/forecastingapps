import pandas as pd
import datetime
from datetime import datetime as dt
from dateutil.relativedelta import *

class TimeBasedCV(object):
    '''
    Parameters 
    ----------
    train: int
        number of time units to include in each train set
        default is 7
    freq: string
        frequency of input parameters. possible values are: days, months, years, weeks, hours, minutes, seconds
        possible values designed to be used by dateutil.relativedelta class
        deafault is days
    '''
    
    
    def __init__(self, period=7, freq='days',gap=0):
        self.period = period
        self.freq = freq
        self.gap = gap

        
        
    def split(self, df, date_column='record_date',debug=False):
        '''
        Generate indices to split data into training and test set
        
        Parameters 
        ----------
        data: pandas DataFrame
            your data, contain one column for the record date 
        date_column: string, deafult='record_date'
            date of each record
        Returns 
        -------
        train_index ,test_index: 
            list of tuples (train index, test index) similar to sklearn model selection
        '''
        
        # check that date_column exist in the data:
        try:
            df[date_column]
        except:
            raise KeyError(date_column)
        
        period = self.period
        gap = self.gap
        df[self.freq] = 1
        
        df.set_index(date_column,inplace=True)
        df.sort_index(inplace=True)
        
        dfre = df.days.resample("1d").sum()
        dfre = dfre[dfre!=0] #removing days without data
        
        end_test=0
        mod = 1
        
        total = int(dfre.count()/period)
        if(debug):
            print(dfre.count())
            print(total)           
        train_indices_list = []
        test_indices_list = []
        train_days_list = []
        test_days_list = []

        
        last_split=0
        while end_test < total:
            train = dfre.iloc[gap:period*mod]
            test =  dfre.iloc[period*mod:period+period*mod]

            train_start, train_stop, train_days = train.index.min().date(),train.index.max().date(),train.count() 
            test_start,test_stop, test_days = test.index.min().date(), test.index.max().date(),test.count()

            if debug: print(f"Split {mod} : Train/Test {train_days,test_days} days  {train_start} to  {train_stop} / {test_start} to  {test_stop} ")
            end_test = mod
            mod+=1

            train_indices_list.append(df.truncate(train_start,train_stop).index.to_list())
            
            if(end_test == total):last_split =1
            test_indices_list.append(df.truncate(test_start,test_stop+pd.Timedelta(f"{last_split}d")).index.to_list())
            
            train_days_list.append(train_days)
            test_days_list.append(test_days)
            
            


        # mimic sklearn output  
        index_output = [(train,test,n_train_days, n_test_days) for train,test,n_train_days, n_test_days in zip(train_indices_list,test_indices_list,train_days_list,test_days_list)]

        self.n_splits = total
        
        return index_output
    
    
    def get_n_splits(self):
        """Returns the number of splitting iterations in the cross-validator
        Returns
        -------
        n_splits : int
            Returns the number of splitting iterations in the cross-validator.
        """
        return self.n_splits 